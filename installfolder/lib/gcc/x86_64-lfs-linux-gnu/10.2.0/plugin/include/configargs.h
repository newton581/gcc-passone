/* Generated automatically. */
static const char configuration_arguments[] = "../configure --target=x86_64-lfs-linux-gnu --prefix=/workspace/binutils/installfolder --with-glibc-version=2.11 --with-sysroot=/workspace/binutils/installfolder --with-newlib --without-headers --enable-initfini-array --disable-nls --disable-shared --disable-multilib --disable-decimal-float --disable-threads --disable-libatomic --disable-libgomp --disable-libquadmath --disable-libssp --disable-libvtv --disable-libstdcxx --enable-languages=c,c++";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
